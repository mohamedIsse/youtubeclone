const User = require('../../models').User;
const authService = require('../services/authService');

console.log(User)
const signUp = async (req,res,next) => {
    try{
        const {email, username, password} = req.body;

        const hashedPassword = await authService.hashPassword(password);
        await User.create({
            username,
            email,
            password: hashedPassword
        })
        console.log("User does get created")
        return res.status(201).json({message: "User sign up was successfull"})
    }
    catch(err){
        console.log('Error on signup ', err);
        res.status(400).json({
            error: err,
        });
        next(err);
    }
}

const signIn = async (req,res,next) => {
    try{
        const user = await User.findAll({
            where: {
                username: req.body.username
            }
        })

        if(user.length === 0){
            return res.status(404).send({message: "User not found"})
        }

        const passwordIsCorrect = await authService.verifyPassword(user[0].dataValues, req.body.password);

        if (!(user && passwordIsCorrect)) {
            return res.status(401).json({
              error: 'Invalid username or password',
            });
        }
        
        const token = await authService.generateToken(user);

        return res.status(200).send({ token, name: user[0].dataValues.name });
    }
    catch(err){
        console.log('Error upon signing in', err);
        res.status(400).json({
            error: err,
        });
        next(err);
    }
}

module.exports = {
    signUp,
    signIn,
  };
  