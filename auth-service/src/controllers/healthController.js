const healthCheck = async (req, res) => {
    return res.json({ status: 'up' });
  };
  
  module.exports = {
      healthCheck
  }
  