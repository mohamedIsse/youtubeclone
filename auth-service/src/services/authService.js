const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();

module.exports = {
    hashPassword(password){
        const salt = bcrypt.genSaltSync(10);
        const passwordHash = bcrypt.hashSync(password, salt);
        return passwordHash
    },
    verifyPassword(user, password){
        let passwordIsCorrect = false;
        if(user){
            passwordIsCorrect = bcrypt.compareSync(password, user.password)
        }
        return passwordIsCorrect
    },
    async generateToken(user){
        const userToken = {
            username: user.name,
            id: user.id
        };

        const token = await jwt.sign(userToken, process.env.JWT_SECRET, { expiresIn: '1d' });
        return token;
    }
}