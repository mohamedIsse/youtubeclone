const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const authRoutes = require('./src/routes/auth');


require('dotenv').config();

const app = express();
const port = process.env.PORT || 3000;

app.use(morgan('dev'));
app.use(bodyParser.json());

app.use(authRoutes);

app.listen(port, () => console.log(`Server is listening at port: ${port}`));