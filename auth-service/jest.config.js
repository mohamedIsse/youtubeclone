// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {

    coverageDirectory: 'coverage',
    globalSetup: './src/setupTests.js',
  
    // A path to a module which exports an async function that is triggered once after all test suites
    globalTeardown: './src/teardownTests.js',
  
    // The test environment that will be used for testing
    testEnvironment: 'node',
  };
  