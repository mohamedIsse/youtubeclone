'use strict';
const {
  Model
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {};
  
  User.init({
    username:{
      type:DataTypes.STRING,
      allowNull:false
    },
    email: {
      type:DataTypes.STRING,
      allowNull:false
    },
    password: {
      type:DataTypes.STRING,
      allowNull:false
    },
    photo: DataTypes.STRING
  }, {
    sequelize,
    modelName: "User",
    tableName: "User"
  });

  return User;
};